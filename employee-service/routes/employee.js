const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/', (request, response) => {
  const { id,ename,salary,age } = request.body

  const query = `
    INSERT INTO employee
      (id, ename,salary,age)
    VALUES
      ('${id}','${ename}','${salary}','${age}')
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get('/', (request, response) => {
  const query = `
    SELECT *
    FROM employee
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.put('/:id', (request, response) => {
  const { id } = request.params
  const { ename,salary,age } = request.body

  const query = `
    UPDATE employee
    SET
      ename = '${ename}', 
      salary = '${salary}',
      age='${age}'
    WHERE
      Id = ${id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/:id', (request, response) => {
  const { id } = request.params

  const query = `
    DELETE FROM employee
    WHERE
      Id = ${id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router
